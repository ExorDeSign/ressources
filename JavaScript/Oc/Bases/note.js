\n 	//retour à la ligne

console.log("Je suis une chaîne".length); // Affiche 18

var mot = "Kangourou";
var longueurMot = mot.length; //propriété
console.log(longueurMot); // Affiche 9

var motEnMinuscules = mot.toLowerCase(); //méthode
console.log(motEnMinuscules); // Affiche en minuscule

var motEnMajuscules = mot.toUpperCase(); //méthode
console.log(motEnMajuscules); // Affiche en maJ

var chaine = "azerty";
console.log(chaine === "azerty"); // Affiche true
console.log(chaine === "qwerty"); // Affiche false

var sport = "Tennis-ballon"
console.log(sport.charAt(0)); // Affiche "T" 	indice(0)
console.log(sport[0]); // Affiche "T"			indice(0)
console.log(sport.charAt(6)); // Affiche "-"	indice(6)
console.log(sport[6]); // Affiche "-"			indice(6)
-----------------------------------------------------------
var prenom = prompt('Entrez votre prenom');
var nom = prompt('Entrez votre nom');
alert('Bonjour, ' + prenom + ' ' + nom);

console.log(Math.min(4.5, 5)); // renvoie le minimum des nombres passés en paramètres
console.log(Math.random()); // Affiche un nombre aléatoire entre 0 et 1
console.log(Math.PI()); // PI (3.14)
-----------------------------------------------------------
var tvaHT=Number(prompt("Entrez votre valeur"));
var tvaTTC=tvaHT*0.196;
console.log(tvaTTC);
-----------------------------------------------------------
=== Egal à
!== Différent de
< Inférieur à
<= Inférieur ou égal à
> Supérieur à
>= Supérieur ou égal à
-----------------------------------------------------------
if (condition) {
	 // instructions exécutées quand la condition est vraie
}
------------------------------------------------------------
if (true) {
	 // la condition du if est toujours vraie :
	// les instructions de ce bloc seront toujours exécutées
}

if (false) {
	// la condition du if est toujours fausse :
	// les instructions de ce bloc ne seront jamais exécutées
}
--------------------------------------------------------------
if ( ) { } 
else if ( ){ } 
else if ( ){ } 
else { }
--------------------------------------------------------------
switch (expression) {
	case valeur1: //obligatoirement une valeur et non une condition
		// instructions exécutées quand expression vaut valeur1
	break;

	case valeur2:
		// instructions exécutées quand expression vaut valeur2
	break;
	...
	
	default:
		// instructions exécutées quand aucune des valeurs ne correspond
}
---------------------------------------------------------------
while (condition) {
	// instructions exécutées tant que la condition est vérifiée
}
----------------------------------------------------------------
for (initialisation; condition; étape) {
    // instruction executées tant que la condition est vérifiée
}		
/*Si on peut prévoir à l'avance le nombre de tours de boucles à effectuer, 
	la boucle for est le meilleur choix. Sinon, il vaut mieux utiliser le while.*/

---------------------------------------------------------------
// Déclaration d'une fonction nommée maFonction
function maFonction() {
// Instructions de la fonction
} 
/*
	Rien n'oblige à récupérer la valeur de retour d'une fonction, mais dans ce cas, 
cette valeur est "oubliée" par le programme qui appelle la fonction ! Si on essaie de 
récupérer la valeur de retour d'une fonction qui n'inclut pas d'instruction return, on obtient undefined.
	Une fonction qui ne renvoie pas de valeur est parfois appelée une procédure.
	Les variables déclarées dans le corps d'une fonction sont appelées des variables locales, elles ne sont
utilisables qu'à l'intérieur de la fonction.
*/
---------------------------------------------------------------
// Déclaration de la fonction maFonction
function maFonction(param1, param2, ...) {
    // Instructions pouvant utiliser param1, param2, ...
}	
// Appel de la fonction maFonction
// param1 reçoit la valeur de arg1, param2 la valeur de arg2, ...
maFonction(arg1, arg2, ...);
---------------------------------------------------------------


