/**
 * Rappel function Js pour string
 */
console.log("ABC".length); // Affiche 3
console.log("Je suis une chaîne".length); // Affiche 18

let mot = "Kangourou";
let longueurMot = mot.length; // longueurMot contient la valeur 9
    console.log(longueurMot); // Affiche 9

let motInitial = "Bora-Bora";
let motEnMinuscules = motInitial.toLowerCase();
    console.log(motEnMinuscules); // Affiche "bora-bora"
let motEnMajuscules = motInitial.toUpperCase();
    console.log(motEnMajuscules); // Affiche "BORA-BORA"

let chaine = "azerty";
    console.log(chaine === "azerty"); // Affiche true
    console.log(chaine === "qwerty"); // Affiche false

    console.log("Azerty" === "azerty"); // Affiche false à cause du A majuscule

let valeurSaisie = "Quitter";
    console.log(valeurSaisie === "quitter"); // Affiche false à cause du Q majuscule
    console.log(valeurSaisie.toLowerCase() === "quitter"); // Affiche true

let sport = "Tennis-ballon"; // 13 caractères
    console.log(sport.charAt(0)); // Affiche "T"
    console.log(sport[0]); // Affiche "T"
    console.log(sport.charAt(6)); // Affiche "-"
    console.log(sport[6]); // Affiche "-"

let longueurSport = sport.length;
    console.log(sport[longueurSport - 1]); // Affiche "n"
    console.log(sport[longueurSport]); // Affiche undefined : longueur dépassée !

let prenom = "Odile"; // 5 caractères
    console.log(prenom[0]);
    console.log(prenom[1]);
    console.log(prenom[2]);
    console.log(prenom[3]);
    console.log(prenom[4]);

for (let i = 0; i < prenom.length; i++) {
    console.log(prenom[i]);
}

/**
 * Consonnes & voyelles
 *
 */
let mot = String(prompt("Entrez un mot :"));
console.log('Se mot contient : ' + compterNbVoyelles(mot) + ' voyelles');
console.log('Se mot contient : ' + compterNbConsonnes(mot) + ' consonnes');
console.log('Mot inversé : ' + inverser(mot));
console.log('Palindrome : ' + palindrome(mot));

function compterNbVoyelles(mot) {
    let nbVoyelles = 0;
    for (let i = 0; i < mot.length; i++) {
        let lettre = mot[i].toLowerCase();
        if ((lettre === 'a') || (lettre === 'e') || (lettre === 'i') ||
            (lettre === 'o') || (lettre === 'u') || (lettre === 'y')) {
            nbVoyelles++;
        }
    }
    return nbVoyelles;
}

function compterNbConsonnes(mot) {
    let nbConsonnes = 0;
    for (let i = 0; i < mot.length; i++) {
        nbConsonnes++
    }
    return nbConsonnes - compterNbVoyelles(mot);
}

// Renvoie le mot inverser
function inverser(mot) {
    let motInverse = "";

    // Solution 1 : ajouter chaque lettre au début du mot inversé
    for (let i = 0; i < mot.length; i++) {
        motInverse = mot[i] + motInverse;
    }

    // Solution 2 : parcourir le mot de la fin vers le début
    for (let i = mot.length - 1; i >= 0; i--) {
        motInverse = motInverse + mot[i];
    }
    return motInverse;
}

// Vérifie le Palindrome
function palindrome(mot) {
    let motNotInvers = mot.toLowerCase();
    let motInvers = inverser(mot).toLowerCase();

    if (motNotInvers === motInvers) {
        return ('Il s\'agit d\'un palindrome')
    } else {
        return ('Il ne s\'agit pas d\'un palindrome')
    }
}
// Plus simple
// if (motInverse.toLowerCase() === mot.toLowerCase()) {
//     console.log("C'est un palindrome");
// } else {
//     console.log("Ce n'est pas un palindrome");
// }


// Leet speak
// Renvoie un mot converti en "leet speak"
function convertirEnLeetSpeek(mot) {
    let motLeet = "";
    for (let i = 0; i < mot.length; i++) {
        motLeet = motLeet + trouverLettreLeet(mot[i]);
    }
    return motLeet;
}

// Renvoie l'équivalent "leet speak" d'une lettre
function trouverLettreLeet(lettre) {
    // Par défaut, la lettre ne change pas
    let lettreLeet = lettre;
    switch (lettre.toLowerCase()) {
        case "a":
            lettreLeet = "4";
            break;
        case "b":
            lettreLeet = "8";
            break;
        case "e":
            lettreLeet = "3";
            break;
        case "l":
            lettreLeet = "1";
            break;
        case "o":
            lettreLeet = "0";
            break;
        case "s":
            lettreLeet = "5";
            break;
        case "t":
            lettreLeet = "7";
            break;
        // ...
    }
    return lettreLeet;
}