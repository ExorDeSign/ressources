/**
 * Personnage Jdr
 */
let perso = {
    nom: "Aurora",
    sante: 150,
    force: 25,
    xp: 0,

    // methode clé: fonction dans l'objet
    decrire: function () {
        let description = this.nom + " a " + this.sante + " points de vie, " +
            this.force + " en force et " + this.xp + " d\'xp";
        return description;
    }
};

// Renvoie la description d'un personnage avec parametres
function decrire(personnage) {
    let description = personnage.nom + " a " + personnage.sante + " points de vie et " + personnage.force + " en force";
    return description;
}
// Appel fonction avec parametres
console.log(decrire(perso));

// Fonction methode
console.log(perso.decrire());
// Aurora est blessée par une flèche
perso.sante = perso.sante - 20;

// Aurora trouve un bracelet de force
perso.force = perso.force + 10;

// Aurora apprend une nouvelle compétence
perso.xp = perso.xp + 15;

console.log(perso.decrire());


/**
 * Calcul aire & perimètre
 */
let r = Number(prompt("Entrez le rayon d'un cercle :"));
let cercle = {
    rayon: r,

    aire: function () {
        return this.rayon * this.rayon * Math.PI;
    },

    perimetre: function () {
        return 2 * this.rayon * Math.PI;
    }
};

console.log("Son périmètre vaut " + cercle.perimetre());
console.log("Son aire vaut " + cercle.aire());

/**
 * Compte
 */
let compte = {
    titulaire: "Alex",
    solde: 0,

    // Crédite le compte d'un certain montant
    crediter: function (montant) {
        this.solde = this.solde + montant;
    },
    // Débite le compte d'un certain montant
    debiter: function (montant) {
        this.solde = this.solde - montant;
    },
    // Renvoie la description du compte
    decrire: function () {
        let description = "Titulaire : " + this.titulaire +
            ", solde : " + this.solde + " euros";
        return description;
    }
};

console.log(compte.decrire());
let credit = Number(prompt("Entrez le montant à créditer :"));
compte.crediter(credit);
let debit = Number(prompt("Entrez le montant à débiter :"));
compte.debiter(debit);
console.log(compte.decrire());