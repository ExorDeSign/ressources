/* 
Activité : jeu de devinette
*/

// NE PAS MODIFIER OU SUPPRIMER LES LIGNES CI-DESSOUS
// COMPLETEZ LE PROGRAMME UNIQUEMENT APRES LE TODO

console.log("Bienvenue dans ce jeu de devinette !");

// Cette ligne génère aléatoirement un nombre entre 1 et 100
var solution = Math.floor(Math.random() * 100) + 1;

var answer = Number(prompt("Entrez un nombre !"));

for (var i = 0; answer !== solution && i < 5; i++)
{
    if (answer < solution)
    {
        console.log(answer +" est trop petit");
    }
    else
    {
        console.log(answer +" est trop grand");
    }
    answer = Number(prompt("Proposez une autre réponse"));
}

if(answer === solution)
{
    console.log("Bravo, la réponse était bien "+solution);
    console.log("Vous avez trouvé en "+(i+1)," essais!");
}
else
{
    if (answer < solution)
    {
        console.log(answer +" est trop petit");
    }
    else
    {
        console.log(answer +" est trop grand");
    }
    console.log("Dommage, vous n'avez pas réussi à deviner le nombre, retentez votre chance.");
    console.log("La bonne réponse était: "+solution)
}
// Décommentez temporairement cette ligne pour mieux vérifier le programme
//console.log("(La solution est " + solution + ")");

// TODO : complétez le programme