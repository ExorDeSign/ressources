
export class Model {

    private reference : string;
    private name : string;
    private state : number;
    public animateState: String;

    constructor (data: any) {
        this.reference = data.reference;
        this.name = data.name;
        this.state = data.state;
        this.animateState='created';
    }

    toggleAnimateState() {
        this.animateState = this.animateState === 'created' ? 'active' : 'created';
    }
}