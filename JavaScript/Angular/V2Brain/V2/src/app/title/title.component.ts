import {Component, Input, OnInit} from '@angular/core';
// import {Config} from '../config';

@Component({
  selector: 'my-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {
  @Input() title: string;

  constructor() {
    // this.title = Config.APP_TITLE;
  }

  ngOnInit() {
  }

}
