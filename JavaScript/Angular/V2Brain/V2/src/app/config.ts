import {Model} from './models/item';

// export const essai:string = 'bonjour';
export class Config {
    public static get APP_TITLE(): string {
        return 'Application Test 1'
    }

    public static get APP_VERSION(): string {
        return 'Version 1.0'
    }

    public static get FAKE_COLLECTION(): [Model] {
        return [
            new Model ({reference: "012", name: "Denis", state: 0}),
            new Model ({reference: "013", name: "Robert", state: 1}),
            new Model ({reference: "014", name: "Nicole", state: 2})
        ]
    }
}