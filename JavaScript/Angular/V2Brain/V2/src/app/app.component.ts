import { Component } from '@angular/core';
import { Config } from './config';
import { Model } from './models/item';


@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  private title : string;
  private version: string;
  public collection: [Model];

  constructor(){
    this.title = Config.APP_TITLE;
    this.version = Config.APP_VERSION;
    this.collection = Config.FAKE_COLLECTION;
  }

  onCreateModel(model:Model) {
    this.collection.unshift(model);
  }

}
