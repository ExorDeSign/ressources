import {Component, OnInit, EventEmitter, Output, Input} from '@angular/core';
import {Model} from "../models/item";

@Component({
  selector: 'item-form',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})

export class ItemComponent implements OnInit {
    @Input() model;
    @Output() onCreateModel = new EventEmitter();


  constructor() {
    this.resetModel();
  }

  resetModel() {
    this.model = new Model({reference: '', name: '', state: 0});
  }

  createModel(){
    this.onCreateModel.emit(this.model);
    this.resetModel();
  }

  ngOnInit() {
  }
}
