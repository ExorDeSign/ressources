import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  private title : string;
  private version: string;
  public collection: [any];
  public item = null;
  private object : {
    reference : string,
    name : string,
    state : number,
  };

  constructor(){
    //  Initialise avec quelque chose de vrai
    this.title = "Application Test";
    this.version = "V 1.0";
    this.collection = [
      {reference : "012", name :"Denis", state:0},
      {reference : "013", name :"Robert", state:1},
      {reference : "014", name :"Nicole",state:2}
    ];

    // Initialise object
    this.resetObject ();
  }

  resetObject () {
    this.object = {reference : "", name : "", state: 0};
  }

  getDetails (item) {
    // console.log('item', item);
    this.item = item;
  }

  closeItem () {
    this.item = null;
  }

  addElement() {
  this.collection.push(this.object);
  this.resetObject();
  }
}
