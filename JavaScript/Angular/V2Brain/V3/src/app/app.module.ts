import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
/*Modules*/
import { TitleModule } from './title/title.module';
/*Pipes*/
import { ItemPipe } from './pipe/item.pipe';
/*Components*/
import { AppComponent } from './app.component';
import { ItemComponent } from './item/item.component';
import { ListComponent } from './list/list.component';
/*Services*/
import { ItemService } from './services/item.service';
import { CollectionService } from './services/collection.service';

@NgModule({
  declarations: [
    AppComponent,
    ItemPipe,
    ItemComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TitleModule
  ],
  providers: [
    ItemService,
    CollectionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
