import { Component } from '@angular/core';
import { Config } from './config';
import { Model } from './models/item';
import { CollectionService } from './services/collection.service';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  private title: string;
  private version: string;
  public collection: Model[];

  constructor(CollectionService: CollectionService) { /*Injection du service*/
    this.title = Config.APP_TITLE;
    this.version = Config.APP_VERSION;
    this.collection = CollectionService.getCollection();
  }
}
