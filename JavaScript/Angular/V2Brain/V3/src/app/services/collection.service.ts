/*import { Injectable } from '@angular/core';*/
import { Model } from '../models/item';

/*@Injectable() Non obligatoire pour le service car ne reçoit pas d'injections*/
export class CollectionService {
  public collection: Model[];

  constructor() {
    this.collection = [];
  }
  getCollection(): Model[] {
    return this.collection;
  }
}
