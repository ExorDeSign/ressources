import { Injectable } from '@angular/core';
import { Model } from '../models/item';
import { CollectionService } from './collection.service';

@Injectable() /*Le service qui va recevoir des dépendances => CollectionService*/
export class ItemService {
  public collection: Model[];

  constructor(CollectionService: CollectionService) {
    this.collection = CollectionService.getCollection();
  }
  addItemToCollection(model: Model): ItemService {
    this.collection.unshift(model);
    return this;
  }
  // createNewModel() {
  //   return new Model({reference: '', name: '', state: 0});
  // }
}
