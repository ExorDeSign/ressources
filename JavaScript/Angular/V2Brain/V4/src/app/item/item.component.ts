import {Component, OnInit, Input} from '@angular/core';
import { ItemService } from '../services/item.service';
import { Model } from '../models/item';

@Component({
  selector: 'item-form',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})

export class ItemComponent implements OnInit {
    @Input() model;
    public ItemService: ItemService;
    constructor(ItemService: ItemService) {
    this.resetModel();
    this.ItemService = ItemService;
  }
  resetModel() {
    // this.model = this.ItemService.createNewModel();
    this.model = new Model({reference: '', name: '', state: 0});
  }
  createModel() {
    this.ItemService.addItemToCollection(this.model);
    this.resetModel();
  }

  ngOnInit() {
  }
}
