import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'my-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {
  @Input() title: string;
  @Input() version: string;

  constructor() {
  }

  ngOnInit() {
  }

}
